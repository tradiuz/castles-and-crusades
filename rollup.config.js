import copy from 'rollup-plugin-copy';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';
import { defineConfig } from 'rollup';
import livereload from 'rollup-plugin-livereload';
import * as yaml from 'js-yaml';
import { visualizer } from 'rollup-plugin-visualizer';

const name = 'castles_crusades';
const distDirectory = 'dist';
const srcDirectory = 'src';

const staticFiles = ['assets', 'lang', 'packs', 'styles', 'templates', 'system.json'];

const isProd = process.env.NODE_ENV === 'production';
const isDev = !isProd;

const banner = `/**
* Author: Troll Lord Games
* Content License: All Rights Reserved Troll Lord Games
* Software License: Apache License, Version 2.0
*/`;

/**
 * this simple plugin displays which environment we're in when rollup starts
 * @param {string} environment - the environment to display
 */
const environment = (environment) => {
  /** @type {import('rollup').PluginContext} */
  const plugin = {
    name: 'environment',
    buildStart() {
      console.log('\x1b[32m%s%s\x1b[0m', 'Environment: ', environment);
    },
  };
  return plugin;
};

export default defineConfig({
  input: { [`${name}`]: `${srcDirectory}/module/${name}.ts`},
  output: {
    dir: distDirectory,
    format: 'es',
    sourcemap: true,
    assetFileNames: '[name].[ext]',
    banner: banner,
  },
  plugins: [
    environment(process.env.NODE_ENV),
    typescript({ noEmitOnError: false }),
    isProd && terser(),
    copy({
      targets: [
        {
          src: staticFiles.map((f) => `${srcDirectory}/${f}`),
          dest: distDirectory,
        },
        {
          // Convert the template file
          src: [`${srcDirectory}/template.yml`],
          dest: distDirectory,
          transform: (content, srcPath, _dstPath) => {
            const data = yaml.load(content.toString(), { filename: srcPath });
            return JSON.stringify(data, null, 2);
          },
          rename: (name, _ext, _srcPath) => `${name}.json`,
        },
        {
          // Convert the language files
          src: [`${srcDirectory}/lang/*.yml`],
          dest: `${distDirectory}/lang`,
          transform: (content, srcPath, _dstPath) => {
            const data = yaml.load(content.toString(), { filename: srcPath });
            return JSON.stringify(data, null, 2);
          },
          rename: (name, _ext, _srcPath) => `${name}.json`,
        }
                
      ],

    }),
    visualizer(),
    isDev && livereload(distDirectory),
  ],
  strictDeprecations: true
});